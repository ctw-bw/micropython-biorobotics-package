import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="biorobotics-BE",  # Add username to prevent conflicts
    version="0.0.1",
    author="Robert Roos",
    author_email="robert.soor@gmail.com",
    description="Extended features for micropython",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/ctw-bw/micropython-biorobotics-package",
    packages=["biorobotics"],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
