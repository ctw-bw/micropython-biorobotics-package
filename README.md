# Micropython BioRobotics Modules

[![CodeFactor](https://www.codefactor.io/repository/bitbucket/ctw-bw/micropython-biorobotics-package/badge)](https://www.codefactor.io/repository/bitbucket/ctw-bw/micropython-biorobotics-package)
[![Documentation Status](https://readthedocs.org/projects/micropython-dummy-modules-biorobotics/badge/?version=latest)](https://micropython-dummy-modules-biorobotics.readthedocs.io/en/latest/modules.html#biorobotics)

This repo contains a Python package which in turn contains some helpful objects to be used on a MCU. It is made to be run on top of the micropython modules, like `pyboard` and `machine`.

## Submodule

Since these extra classes are to be run on both the microcontroller and in the dummy modules on a PC, these classes were put in their own repository which is included as a git submodule in both.

 *  The micropython fork: https://bitbucket.org/ctw-bw/micropython
 *  The micropython dummy modules: https://bitbucket.org/ctw-bw/micropython-dummy-modules

Install the dummy modules using `pip install` to also make this package available.

## Wiki 

Usage of this package is explained in the wiki of the micropython fork: https://bitbucket.org/ctw-bw/micropython/wiki/Home

## Docs

API Documentation is generated together with the dummy modules: https://micropython-dummy-modules-biorobotics.readthedocs.io/ (see 'Modules' > 'Biorobotics').

## Developing

See [CONTRIBUTING.md](CONTRIBUTING.md)
