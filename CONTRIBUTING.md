# Development notes on the biorobotics package

## Folders and packaging

This package does not have the Pip package format. (That would require a directory named `biorobotics`.) This was done on purpose, such that the dummy modules package can include this package as a Git submodule. 

## Packages

This package needs to run from both the microcontroller and on PC (using the dummy modules).  
The dummy modules are made to shadow the micropython packages like `pyb` and `machine`. However we also need to make sure we don't use extra packages that are available on PC but not on the microcontroller.

## Type Hinting

`typing` is one such package. It is very useful to increase the in-code type hinting, but it is not available in micropython.

We still strive for the most complete type hinting, since this will make developing microcontroller programs from a PC easier. 

## Class and Function Exposing

Inside `__init__.py` is defined which features should be exposed in the package. Everything that is not listed there is not readily available. This init file is executed when the module is being imported.

All files start with an underscore to make them 'private' (only by convention). Intended usage would be:

```
from biorobotics import Ticker

t = Ticker(...)
```

The alternative *and unintended* usage would be:

```
from biorobotics._ticker import Ticker  # <-- Import is not as intended!

t = Ticker(...)
```

By relying on `__init__.py` we can also hide parts of our package from the user.

## Testing

This package is also tested as part of the dummy modules repo, through unit tests. This is convenient, since the dummy modules allow the biorobotics package to run normally.  
